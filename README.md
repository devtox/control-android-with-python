

## Automate Android with Python

Uses Adb to send commands. Needs adb server running.

`android_type("hello world")`

More on Python:

* https://pythonprogramminglanguage.com
* https://pythonbasics.org
